using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ability", menuName = "ScriptableObjects/Ability")]
public class ScriptableAbility : ScriptableObject
{
    public string abilityName;
    public string abiltyDescription;
    public string abilityEffect;
    public float abilityBaseDamage;
}
