using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [SerializeField] private ScriptableBaseStats scriptableStats;

    public float baseHealth;           //Base health for a character
    public float baseMana;             //Base mana for a character
    public float baseStrenght;         //Base strenght for a character
    public float baseAgility;          //Base Agility for a character
    public float baseMagicAbility;     //Base Magic Ability for a character
    public float basePhysicalDefence;  //Base Defence for a character
    public float baseMagicDefence;     //Base Defence for a character

    public virtual void AssignBaseStats()
    {
        baseHealth = scriptableStats.baseHealth;
        baseMana = scriptableStats.baseMana;
        baseStrenght = scriptableStats.baseStrenght;
        baseAgility = scriptableStats.baseAgility;
        baseMagicAbility = scriptableStats.baseMagicAbility;
        basePhysicalDefence = scriptableStats.basePhysicalDefence;
        baseMagicDefence = scriptableStats.baseMagicDefence;
    }
}
